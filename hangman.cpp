// hangman
// Andrew Patterson
// 2017-12-

#include <iostream>
#include <fstream>
#include <string>
#include <ncurses.h>
#include <unistd.h>
#include <ctime>

using namespace std;

enum Direction1d {L, R};

//Gets the word to hangman game w/
string getWord		(string fileName);

int numLines		(string fileName);

//void display		(string word, bool *correct, int wrong, bool chosen[]);
void display		(string word, bool *correct, int wrong, uint32_t chosen);

void mvvline_bd		(int y, int x, int length);
void mvhline_bd		(int y, int x, int length);
void mv45line		(int y, int x, int length, Direction1d direction);

//adding functions for: lines
void drawBoxes		(int vertStartX, int horizStartY);

//word
void writeWord		(string word, bool *correct);

//gibbet
void drawGibbet		(int y, int x, int height, int armWidth);

//person
void drawPerson		(int y, int x, int wrong);

//head
void drawHead		(int y, int x);

//previously chosen letters
void foundLetters	(uint32_t chosen);

int main (int argc, char **argv)
{
	setlocale(LC_ALL, "en_US.UTF-8");
	//Setup stuff
	srand(time(NULL));		//Seed rng
	string word;			//set up the word string
	int wrongGuesses = 0;	//Set the number of wrong guesses so far to 0
	const int MAX_WRONG = 6;//Set max number of wrong guesses allowed
	bool playing = true;	//Set playing to true
	bool win 	= false;	//Set whether won yet

	//ncurses setup
	initscr();				//This starts the window
	keypad(stdscr, true);	//This makes ...weird keys look more normal
	nonl();					//This prevents an extra newline from being put at the end
	cbreak();				//TODO: check this purpose
	noecho();				//DO NOT echo keystrokes
	curs_set(false);		//Don't show the cursor
	
	// Choose random word from a file
	do {
	word = getWord("dict-file");
	//word = getWord("/usr/share/dict/american-english");
	}
	while (word.length() == 0);

	bool *correct = new bool[word.length()] { false };
	//bool chosen[26] = { false };
	uint32_t chosen = 0; 

	for (unsigned int i = 0; i < word.length(); i++)
	{
		if (!isalpha(word[i]))
			correct[i] = true;
	}

	// while (playing)
	while (playing)
	{
		//display stuff
		display(word, correct, wrongGuesses, chosen);

		//Some things to check before doing stuff
		bool isCorrect			= false;
		char letterTry;
		bool possiblyWon		= true;

	 	//read a character
		letterTry = getch();

		//if the letter hasn't been chosen yet
		//if (!chosen[toupper(letterTry) - 'A'])
		if (!(chosen & 1 << (toupper(letterTry) - 'A')))
		{
			//loop through string 
			for (unsigned int i = 0; i < word.length(); i++)
			{
				//If i has already been found, and is what you guessed, then
				if ((correct[i]) && (toupper(word[i]) == toupper(letterTry)))
				{
					//it is correct (so you don't lose a life),
					isCorrect			= true;
				}
				//if the character here is the same as read
				else if (toupper(word[i]) == toupper(letterTry))
				{
					//set not a loss
					isCorrect = true;
					//set char to showing (have array of bools?)
					correct[i] = true;
				}
				if (correct[i] == false)
				{
					possiblyWon = false;
				}
			}

			//If this guess was wrong
			if (!isCorrect)
			{
				//decrement amount of losses left
				wrongGuesses++;
			}

			//if losses == 0
			if (wrongGuesses == MAX_WRONG)
			{
				//game over thing
				playing = false;
				win 	= false;
			}

			//if it's possible to have won
			if (possiblyWon)
			{
				playing = false;
				win		= true;
			}

			//chosen[toupper(letterTry) - 'A'] = true;
			chosen |= 1 << (toupper(letterTry) - 'A');
		}

	}

	clear();

	if (win)
		mvprintw(12, 36, "You won!");
	else
		mvprintw(12, 35, "You lost!");

	mvprintw(13, 33, "The word was:");
	mvprintw(14, 40 - word.length() / 2, word.c_str());
	mvprintw(15, 30, "Hit any key to exit.");
	getch();

	//stuff to do before returning
	delete[] correct;
	endwin();

	return 0;
}

string getWord (string fileName)
{
	string word;							//Result
	string tmp;								//temporary string, for getline loop
	int numberOfWords = numLines(fileName);	//The total number of words, from numLines()

	//File stuff
	ifstream file;
	file.open(fileName);

	//Loop through the lines
	for (int i = 0; i < numberOfWords; i++)
	{
		//Getting the string from each
		getline(file, tmp);
		//If it is the random line wanted, put line in returned value
		if ((i == (rand() % numberOfWords)) && (tmp.length() > 0))
			word = tmp;
	}

	return word;
}

int numLines (string fileName)
{
	int numberOfLines = 0;
	string line;

	//File stuff
	ifstream file;
	file.open(fileName);

	while(std::getline(file, line))
		numberOfLines++;

	return numberOfLines;
}

//void display (string word, bool *correct, int wrong, bool chosen[])
void display (string word, bool *correct, int wrong, uint32_t chosen)
{
	//consts
	const int VERT_LINE_START_X		= 51;
	const int HORIZ_LINE_START_Y	= 19;

	const int GIBBET_Y				= 4;
	const int GIBBET_X				= 64;
	const int GIBBET_HEIGHT			= 13;
	const int GIBBET_WIDTH			= 12;

	//Draw basic instructions
	mvprintw(0, 0, "Type an alphabetical key to guess it.");

	//draw Boxes
	drawBoxes(VERT_LINE_START_X, HORIZ_LINE_START_Y);

	//write the word, w/ underlines and stuff
	writeWord(word, correct);

	//draw the gibbet
	drawGibbet(GIBBET_Y, GIBBET_X, GIBBET_HEIGHT, GIBBET_WIDTH);

	//draw the person / losses counter
	drawPerson(6, 63, wrong);
	
	//TODO: Draw guessed letters!
	foundLetters(chosen);

	refresh();
}

void mvvline_bd	(int y, int x, int length)
{
	for (int i = 0; i < length; i++)
	{
		mvaddch(y+i, x, ACS_VLINE);
	}
}

void mvhline_bd	(int y, int x, int length)
{
	for (int i = 0; i < length; i++)
	{
		mvaddch(y, x+i, ACS_HLINE);
	}
}

void mv45line	(int y, int x, int length, Direction1d direction)
{
	for (int i = 0; i < length; i++)
	{
		if (direction == L)
		{
			mvaddch(y+i, x-i, '/');
		}
		else
		{
			mvaddch(y+i, x+i, '\\');
		}
	}
}

void drawBoxes (int vertStartX, int horizStartY)
{
	//Draw boxes
	mvvline_bd(0, vertStartX, horizStartY);		//vertical line
	mvhline_bd(horizStartY, 0, 80);				//horizontal line
	mvaddch(horizStartY, vertStartX, ACS_BTEE);	//tee at the intersections
}

void writeWord (string word, bool *correct)
{
	//Define the 'x' the word will start at
	int startText = 26 - word.length() / 2;
	const int startY = 9;

	//Draw lines/letters
	for (unsigned int i = 0; i < word.length(); i++)
	{
		//if is alpha and ¬found
		if (isalpha(word[i]) && !correct[i])
			//addch '_'
			mvaddch(startY, startText + i, '_');
		else
		{
			//if alpha and found, turn on underline
			if (isalpha(word[i]) && correct[i])
				attron(A_UNDERLINE);

			//print character if not unguessed alpha
			mvaddch(startY, startText+i, word[i]);
			attroff(A_UNDERLINE);
		}
	}
}

void drawGibbet (int y, int x, int height, int armWidth)
{
	//stuff
	int poleStart = x + armWidth;
	int baseStart = y + height;

	//Gibbet thing  (dang, this game is dark when you think about it)
	//top line
	mvhline_bd(y, x, armWidth);
	//base line
	mvhline_bd(baseStart, poleStart - 7, 11);
	//Pole
	mvvline_bd(y, poleStart, height);
	//angles
	mvaddch(y, poleStart, ACS_URCORNER);	//top left
	mvaddch(y, x, ACS_TTEE);		//top right
	mvaddch(baseStart, poleStart, ACS_BTEE);		//bottom
	//Support bits (sadly no box drawing 45°ish angles.  =()
	mv45line(y + 1, poleStart - 3, 3, R);
	mv45line(baseStart - 3, poleStart - 1, 4, L);
	mv45line(baseStart - 1, poleStart + 1, 2, R);
	//draw the rope
	mvvline_bd(y + 1, x, 1);
}

void drawHead (int y, int x)
{
	mvprintw(y, x, "/-\\");
	mvprintw(y+1, x, "\\_/");
}

void drawPerson	(int y, int x, int wrong)
{
	//Draw hangman thing
		//based on # wrong; maybe make switch overflow useful?  (mwahaha)
	switch (wrong)
	{
		case 6:	mvvline_bd	(y + 4, x + 2, 3);
		case 5:	mvvline_bd	(y + 4, x, 3);
		case 4:	mv45line	(y + 2, x + 2, 2, R);
		case 3: mv45line	(y + 2, x, 2, L);
		case 2: mvvline_bd	(y + 2, x + 1, 3);
		case 1: drawHead	(y, x);
	}
}

void foundLetters(uint32_t chosen)
{
	move(21,0);
	for (uint32_t i = 0; (1 << i) <= (1 << 26); i++)
	{
		printw("  ");
		if ((1 << i) & chosen)
			addch('A' + i);
		else
			addch(' ');
	}
}
